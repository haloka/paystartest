<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

abstract class Repository
{
    /**
     * The model which repository should work with it.
     *
     * @var Model
     */
    protected $model;

    /**
     * query order
     *
     * @var Model
     */
    public $order = [];

    /**
     * it's will set the $model.
     *
     * @param void
     *
     * @return void
     *
     * @throws \ReflectionException
     *
     * @throws \App\Repositories\RepositoryException
     */
    public function __construct()
    {
        $this->setModel($this->model());
    }

    /**
     * get model.
     *
     * @param void
     *
     * @return Model
     */
    protected function getModel()
    {
        return $this->model;
    }

    /**
     * this will return model name.
     *
     * @return string
     *
     * @throws \ReflectionException
     * @throws \App\Repositories\RepositoryException
     */
    protected function model()
    {
        $className = (new \ReflectionClass($this))->getShortName();
        $modelName = 'App\\Models\\' . str_replace("Repository", "", $className);
        if (!class_exists($modelName)) {
            throw new RepositoryException(
                "Class " . $modelName
                . " does not exist. please overriding the model() method with the correct model name."
                . Model::class
            );
        }
        return $modelName;
    }

    /**param
     * set model.
     *
     * @param string
     *
     * @return void
     *
     * @throws \App\Repositories\RepositoryException
     */
    protected function setModel(string $modelName)
    {
        $model = new $modelName();

        if (!$model instanceof Model) {
            throw new RepositoryException(
                "Class " . $model . "must be an instance of"
                . Model::class
            );
        } else {
            $this->model = $model;
        }
    }

    /**
     * get all model's data with pagination.
     *
     * @param void
     *
     * @return  Collection
     */
    public function getAllWithPagination()
    {
        $this->model->setPerPage(config('general.pagination_length'));

        $this->setOrder();

        return $this->model->paginate();
    }

    /**
     * get all model's data.
     *
     * @param void
     *
     * @return  Collection
     */
    public function getAll()
    {
        $this->setOrder();

        return $this->model->get();
    }

    private function setOrder()
    {
        foreach ($this->order as $col => $dir) {
            $this->model = $this->model->orderBy($col, $dir);
        }
    }

    /**
     * get all model's data.
     *
     * @param integer
     *
     * @return  mixed
     *
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * create new  model's data.
     *
     * @param array
     *
     * @return $this->model instance
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * create new  model's data.
     *
     * @param integer
     * @param array
     *
     * @return boolean or
     *
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(int $id, array $data)
    {
        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * create new  model's data.
     *
     * @param integer
     * @param array
     *
     * @return boolean
     *
     * @throws Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function delete(int $id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    /**
     * insert data into the model.
     *
     * @param array
     *
     * @return boolean
     *
     */
    public function insert(array $data)
    {
        return DB::transaction(function() use ($data) {
            return $this->model->insert($data);
        });
    }

    /**
     * get data using wherein method.
     *
     * @param string $columnName
     * @param array
     *
     * @return Collection
     */
    public function whereIn(string $columnName, array $data)
    {
        return $this->model->whereIn($columnName, $data)->get();
    }

    public function sync(int $id, string $tableName, array $data)
    {
        return $this
            ->model
            ->findOrFail($id)
            ->$tableName()
            ->sync($data);
    }

    public function like($field, $data)
    {
        return $this->model->where($field, 'like', '%' . $data . '%')->get();
    }

    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function getByIdOrNull($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getModelFields()
    {
        return $this->model->getFillable();
    }

    public function first()
    {
        return $this->first();
    }
}
