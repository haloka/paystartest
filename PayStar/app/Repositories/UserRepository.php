<?php


namespace App\Repositories;


class UserRepository extends Repository
{
    public function getUserByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }
}
