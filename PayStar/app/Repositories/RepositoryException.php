<?php


namespace App\Repositories;

use Exception;

class RepositoryException extends Exception
{
    public function errorMessage()
    {
        $errorMsg = 'Repository Exception : Error on line ' . $this->getLine()
            . ' in ' . $this->getFile() . ' ' . $this->getMessage();
        return $errorMsg;
    }
}
