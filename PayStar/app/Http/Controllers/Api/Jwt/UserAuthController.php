<?php

namespace App\Http\Controllers\Api\Jwt;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\UserRegisterRequest;
use App\Services\UserAuthService;

class UserAuthController extends Controller
{
    private $userAuthService;

    public function __construct(UserAuthService $userAuthService)
    {
        $this->userAuthService = $userAuthService;
    }

    public function login(UserLoginRequest $request)
    {
        return $this->respondWithToken($this->userAuthService->login($request->validated()));
    }

    public function register(UserRegisterRequest $request)
    {
        return $this->respondWithToken($this->userAuthService->register($request->validated()));
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 1,
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => time() + auth('users')->factory()->getTTL() * 60,
            ]
        ]);
    }
}
