<?php

namespace App\Http\Controllers\Api\Jwt;

use App\Http\Controllers\Controller;
use App\Services\AuthService;

class PublicAuth extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->middleware(['assign.guard:user']);
        $this->authService = $authService;
    }

    public function getMe()
    {
        return [
            'status' => 1,
            'data' => $this->authService->getMe(),
        ];
    }

    public function logout()
    {
        $this->authService->loggout();
        return response()->json([
            'status' => '1',
            'message' => 'Successfully logged out'
        ]);
    }
}
