<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserAuthService
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login($loginData)
    {
        $user = $this->checkLoginData($loginData);
        return $this->tryToCreateToken($user);
    }

    protected function checkLoginData($loginData)
    {
        $user = $this->userService->getUserByEmail($loginData['email']);
        if ($user !== null && Hash::check($loginData['password'], $user->getPassword())){
            return $user;
        }
        return false;
    }

    public function register($data)
    {
        $data['password'] = Hash::make($data['password']);
        $user = $this->userService->create($data);
        return $this->tryToCreateToken($user);
    }

    protected function tryToCreateToken($user)
    {
        try {
            if (!($user instanceof User)) {
                abort(403, 'wrong email or password');;
            }
            return auth('users')->login($user);
        } catch (JWTException $e) {
            abort(500, 'could not create the token');
        }
    }
}
