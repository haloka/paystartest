<?php


namespace App\Services;


use App\Repositories\PostRepository;

class PostService
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getAllWithPagination()
    {
        return $this->postRepository->getAllWithPagination();
    }

    public function create($data)
    {
        $data['user_id'] = auth('users')->user()->id;
        return $this->postRepository->create($data);
    }

    public function getById($id)
    {
        return $this->postRepository->getById($id);
    }

    public function update($id, $data)
    {
        return $this->postRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->postRepository->delete($id);
    }
}
