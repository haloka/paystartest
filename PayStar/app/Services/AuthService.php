<?php


namespace App\Services;


class AuthService
{
    public function getMe()
    {
        return auth()->user();
    }

    public function loggout()
    {
        auth()->logout();
    }
}
