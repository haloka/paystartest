<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/user')->group(function (){
Route::post('/login',[\App\Http\Controllers\Api\Jwt\UserAuthController::class, 'login']);
Route::post('/register', [\App\Http\Controllers\Api\Jwt\UserAuthController::class, 'register']);
Route::apiResource('/post',\App\Http\Controllers\Api\User\PostController::class);
});
